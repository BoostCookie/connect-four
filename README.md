# Connect Four

The code for the solver is just a port of Pascal Pons' code

https://github.com/PascalPons/connect4

### TODO
- [x] allow other game sizes
- [ ] opening book
- [ ] more extensive benchmarks
