use super::constants::WIDTH;
use super::position::PositionT;

#[derive(Copy, Clone)]
struct Move {
    move_bit: PositionT,
    score: u32,
}

impl Move {
    fn new() -> Move {
        Move {
            move_bit: 0,
            score: 0,
        }
    }
}

/*
 * This struct helps sorting the next moves
 *
 * You have to add moves first with their score
 * then you can get them back in decreasing score
 *
 * This class implement an insertion sort that is in practice very
 * efficient for small number of move to sort (max is Position::WIDTH)
 * and also efficient if the move are pushed in approximatively increasing
 * order which can be acheived by using a simpler column ordering heuristic.
 */
pub struct MoveSorter {
    size: usize,
    entries: [Move; WIDTH],
}

impl MoveSorter {
    pub fn new() -> MoveSorter {
        MoveSorter {
            size: 0,
            entries: [Move::new(); WIDTH],
        }
    }

    /*
     * Add a move in the container with its score.
     * You cannot add more than Position::WIDTH moves
     */
    pub fn add(&mut self, new_move: PositionT, score: u32) {
        let mut pos = self.size;
        self.size += 1;
        while pos != 0 && self.entries[pos - 1].score > score {
            self.entries[pos] = self.entries[pos - 1];
            pos -= 1;
        }
        self.entries[pos].move_bit = new_move;
        self.entries[pos].score = score;
    }
}

/*
 * Get all the moves
 * The highest scoring ones are returned first
 */
impl Iterator for MoveSorter {
    type Item = PositionT;

    fn next(&mut self) -> Option<PositionT> {
        if self.size != 0 {
            self.size -= 1;
            Some(self.entries[self.size].move_bit)
        } else {
            None
        }
    }
}
