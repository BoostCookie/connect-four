use super::*;
use test::Bencher;

#[bench]
fn bench_possible_non_losing_moves(b: &mut Bencher) {
    let pos = test_pos();
    b.iter(move || {
        pos.possible_non_losing_moves();
    });
}

#[bench]
fn bench_move_score(b: &mut Bencher) {
    let pos = test_pos();
    let possible = pos.possible_non_losing_moves();
    b.iter(move || {
        for col in 0..WIDTH {
            let move_bit = possible & position::column_mask(col);
            pos.move_score(move_bit);
        }
    });
}

#[bench]
fn bench_can_win_next(b: &mut Bencher) {
    let pos = test_pos();
    b.iter(move || {
        pos.can_win_next();
    });
}

#[bench]
fn bench_is_winning_move(b: &mut Bencher) {
    let pos = test_pos();
    b.iter(move || {
        for col in 0..WIDTH {
            pos.is_winning_move(col);
        }
    });
}

#[bench]
fn bench_column_mask(b: &mut Bencher) {
    b.iter(move || {
        for col in 0..WIDTH {
            position::column_mask(col);
        }
    });
}

#[bench]
fn bench_can_play(b: &mut Bencher) {
    let pos = test_pos();
    b.iter(move || {
        for col in 0..WIDTH {
            pos.can_play(col);
        }
    });
}
