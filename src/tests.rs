extern crate test;

//mod fast_benches;

use super::*;
use test::Bencher;

fn test_pos() -> Position {
    let mut pos = Position::empty();
    let seq = vec![3,3,3,3,3,0,1,2,0,4,0,6,0];
    for col in seq {
        pos.play_col(col);
    }
    pos
}

#[test]
fn from_key() {
    let pos = test_pos();
    assert_eq!(pos, Position::from_key(pos.key()));
}

#[bench]
fn bench_play_col(b: &mut Bencher) {
    let mut pos = test_pos();
    b.iter(move || {
        for col in 0..WIDTH {
            pos.play_col(col);
        }
    });
}

#[bench]
fn bench_solve_weak(b: &mut Bencher) {
    let pos = test_pos();
    let mut solver = Solver::new();
    b.iter(move || {
        solver.solve(&pos, true);
    });
}

#[bench]
fn bench_solve_strong(b: &mut Bencher) {
    let pos = test_pos();
    let mut solver = Solver::new();
    b.iter(move || {
        solver.solve(&pos, false);
    });
}

#[bench]
fn bench_analyse_weak(b: &mut Bencher) {
    let pos = test_pos();
    let mut solver = Solver::new();
    b.iter(move || {
        for score_opt in solver.analyse(&pos, true) {
            print!("{:?}", score_opt);
        }
        println!();
    });
}

#[bench]
fn bench_analyse_strong(b: &mut Bencher) {
    let pos = test_pos();
    let mut solver = Solver::new();
    b.iter(move || {
        for score_opt in solver.analyse(&pos, false) {
            print!("{:?}", score_opt);
        }
        println!();
    });
}
