use super::constants::{HEIGHT, WIDTH};

use std::fmt;

/*
 * A struct storing a Connect 4 position.
 * Functions are relative to the current player to play.
 * Position containing alignment are not supported by this class.
 *
 * A binary bitboard representationis used.
 * Each column is encoded on HEIGH+1 bits.
 *
 * Example of bit order to encode for a 7x6 board
 * .  .  .  .  .  .  .
 * 5 12 19 26 33 40 47
 * 4 11 18 25 32 39 46
 * 3 10 17 24 31 38 45
 * 2  9 16 23 30 37 44
 * 1  8 15 22 29 36 43
 * 0  7 14 21 28 35 42
 *
 * Position is stored as
 * - a bitboard "mask" with 1 on any color stones
 * - a bitboard "current_player" with 1 on stones of current player
 *
 * "current_player" bitboard can be transformed into a compact and non ambiguous key
 * by adding an extra bit on top of the last non empty cell of each column.
 * This allow to identify all the empty cells whithout needing "mask" bitboard
 *
 * current_player "x" = 1, opponent "o" = 0
 * board     position  mask      key       bottom
 *           0000000   0000000   0000000   0000000
 * .......   0000000   0000000   0001000   0000000
 * ...o...   0000000   0001000   0010000   0000000
 * ..xx...   0011000   0011000   0011000   0000000
 * ..ox...   0001000   0011000   0001100   0000000
 * ..oox..   0000100   0011100   0000110   0000000
 * ..oxxo.   0001100   0011110   1101101   1111111
 *
 * current_player "o" = 1, opponent "x" = 0
 * board     position  mask      key       bottom
 *           0000000   0000000   0001000   0000000
 * ...x...   0000000   0001000   0000000   0000000
 * ...o...   0001000   0001000   0011000   0000000
 * ..xx...   0000000   0011000   0000000   0000000
 * ..ox...   0010000   0011000   0010100   0000000
 * ..oox..   0011000   0011100   0011010   0000000
 * ..oxxo.   0010010   0011110   1110011   1111111
 *
 * key is an unique representation of a board key = position + mask + bottom
 * in practice, as bottom is constant, key = position + mask is also a non-ambigous representation of the position.
 */

/*
 * Generate a bitmask containing one for the bottom slot of each colum
 * must be defined outside of the class definition to be available at compile time for bottom_mask
 */

pub trait PositionTHelperTrait<const N: usize> {
    type Ty;
}
impl PositionTHelperTrait<8> for () {
    type Ty = u8;
}
impl PositionTHelperTrait<16> for () {
    type Ty = u16;
}
impl PositionTHelperTrait<32> for () {
    type Ty = u32;
}
impl PositionTHelperTrait<64> for () {
    type Ty = u64;
}
impl PositionTHelperTrait<128> for () {
    type Ty = u128;
}
pub type PositionT = <() as PositionTHelperTrait<{
    let size = (WIDTH * (HEIGHT + 1)).next_power_of_two();
    assert!(size <= 128, "WIDTH * (HEIGHT + 1) is too large");
    if size >= 8 {
        size
    } else {
        8
    }
},>>::Ty;

const fn bottom(width: usize, height: usize) -> PositionT {
    match width {
        0 => 0,
        _ => bottom(width - 1, height) | (1 << ((width - 1) * (height + 1))),
    }
}

// static bitmaps
const BOTTOM_MASK: PositionT = bottom(WIDTH, HEIGHT);
const BOARD_MASK: PositionT = BOTTOM_MASK * ((1 << HEIGHT) - 1);

// return a bitmask containg a single 1 corresponding to the top cel of a given column
fn top_mask_col(col: usize) -> PositionT {
    1 << ((HEIGHT - 1) + col * (HEIGHT + 1))
}

// return a bitmask containg a single 1 corresponding to the bottom cell of a given column
fn bottom_mask_col(col: usize) -> PositionT {
    1 << (col * (HEIGHT + 1))
}

// return a bitmask 1 on all the cells of a given column
pub fn column_mask(col: usize) -> PositionT {
    ((1 << HEIGHT) - 1) << (col * (HEIGHT + 1))
}

#[inline(always)]
fn compute_winning_position_helper(position: PositionT) -> PositionT {
    // vertical
    let mut r = (position << 1) & (position << 2) & (position << 3);

    // horizontal
    let mut p = (position << (HEIGHT + 1)) & (position << (2 * (HEIGHT + 1)));
    r |= p & (position << (3 * (HEIGHT + 1)));
    r |= p & (position >> (HEIGHT + 1));
    p = (position >> (HEIGHT + 1)) & (position >> (2 * (HEIGHT + 1)));
    r |= p & (position << (HEIGHT + 1));
    r |= p & (position >> (3 * (HEIGHT + 1)));

    // diagonal 1
    p = (position << HEIGHT) & (position << (2 * HEIGHT));
    r |= p & (position << (3 * HEIGHT));
    r |= p & (position >> HEIGHT);
    p = (position >> HEIGHT) & (position >> (2 * HEIGHT));
    r |= p & (position << HEIGHT);
    r |= p & (position >> (3 * HEIGHT));

    // diagonal 2
    p = (position << (HEIGHT + 2)) & (position << (2 * (HEIGHT + 2)));
    r |= p & (position << (3 * (HEIGHT + 2)));
    r |= p & (position >> (HEIGHT + 2));
    p = (position >> (HEIGHT + 2)) & (position >> (2 * (HEIGHT + 2)));
    r |= p & (position << (HEIGHT + 2));
    r |= p & (position >> (3 * (HEIGHT + 2)));
    r
}

/*
 * @param position, a bitmap of the player to evaluate the winning pos
 * @param mask, a mask of the already played spots
 *
 * @return a bitmap of all the winning free spots making an alignment
 */
fn compute_winning_position(position: PositionT, mask: PositionT) -> PositionT {
    compute_winning_position_helper(position) & (BOARD_MASK ^ mask)
}

pub fn bitmask_to_string(bitmask: PositionT) -> String {
    let mut ret = String::from(" ");
    for _ in 0..(2 * WIDTH - 1) {
        ret.push('_');
    }
    ret.push_str(" \n");
    for row in (0..(HEIGHT + 1)).rev() {
        ret.push('|');
        for col in 0..WIDTH {
            let cell = 1 << (row + col * (HEIGHT + 1));
            ret.push_str(match bitmask & cell {
                0 => "0",
                _ => "1",
            });

            if col != WIDTH - 1 {
                ret.push(' ');
            }
        }
        ret.push('|');
        // show the non-relevant bits outside of the mask
        // these should all be zero
        for col in WIDTH..((PositionT::BITS as usize - 1) / (HEIGHT + 1) + 1) {
            let shift = row + col * (HEIGHT + 1);
            if shift < PositionT::BITS as usize {
                let cell: PositionT = 1 << shift;
                ret.push_str(match bitmask & cell {
                    0 => "0",
                    _ => "1",
                });
            }
        }
        ret.push('\n');
    }
    ret.push(' ');
    for _ in 0..(2 * WIDTH - 1) {
        ret.push('‾');
    }
    ret.push(' ');
    ret
}

#[derive(Clone, PartialEq)]
pub struct Position {
    current_position: PositionT,  // bitmap of the current_player stones
    mask: PositionT,              // bitmap of all the already palyed spots
    moves: i32,              // number of moves played since the beginning of the game.
}

impl Position {
    /*
     * Default constructor; Build an empty position
     */
    pub fn empty() -> Position {
        Position {
            current_position: 0,
            mask: 0,
            moves: 0,
        }
    }

    #[allow(dead_code)]
    pub fn from_key(key: PositionT) -> Position {
        let mut mask = 0;
        let mut prev = key + BOTTOM_MASK;
        for c in (0..WIDTH).rev() {
            let column = prev >> (c * (HEIGHT + 1));
            let mask_column = (column + 1).next_power_of_two() / 2 - 1;
            mask |= mask_column << (c * (HEIGHT + 1));
            let column_mask_c = column_mask(c);
            prev &= !((column_mask_c << 1) | column_mask_c);
        }
        Position {
            current_position: key - mask,
            mask,
            moves: mask.count_ones() as i32,
        }
    }

    #[allow(dead_code)]
    pub fn from_readable_key(readable_key: PositionT) -> Position {
        Position::from_key(readable_key - BOTTOM_MASK)
    }

    /*
     * @return positions of player1 and player2
     */
    pub fn positions(&self) -> (PositionT, PositionT) {
        if self.moves % 2 == 0 {
            (self.current_position, self.current_position ^ self.mask)
        } else {
            (self.current_position ^ self.mask, self.current_position)
        }
    }

    /*
     * @return number of moves played from the beginning of the game.
     */
    pub fn nb_moves(&self) -> i32 {
        self.moves
    }

    /*
     * @return a compact representation of a position on WIDTH*(HEIGHT+1) bits.
     */
    pub fn key(&self) -> PositionT {
        self.current_position + self.mask
    }

    /*
     * @return a compact representation of a position on WIDTH*(HEIGHT+1) bits
     *         with BOTTOM_MASK added to make it more readable and similar to
     *         the example here:
     *         http://blog.gamesolver.org/solving-connect-four/06-bitboard/
     */
    pub fn readable_key(&self) -> PositionT {
        self.current_position + self.mask + BOTTOM_MASK
    }

    /*
     * Indicates whether a column is playable.
     * @param col: 0-based index of column to play
     * @return true if the column is playable, false if the column is already full.
     */
    pub fn can_play(&self, col: usize) -> bool {
        (self.mask & top_mask_col(col)) == 0
    }

    /*
     * Plays a possible move given by its bitmap representation
     *
     * @param move_bit: a possible move given by its bitmap representation
     *        only one bit of the bitmap should be set to 1
     *        the move should be a valid possible move for the current player
     */
    pub fn play(&mut self, move_bit: PositionT) {
        self.current_position ^= self.mask;
        self.mask |= move_bit;
        self.moves += 1;
    }

    /*
     * Plays a playable column.
     * This function should not be called on a non-playable column or a column making an alignment.
     *
     * @param col: 0-based index of a playable column.
     */
    pub fn play_col(&mut self, col: usize) {
        self.play((self.mask + bottom_mask_col(col)) & column_mask(col));
    }

    /*
     * Bitmap of the next possible valid moves for the current player
     * Including losing moves.
     */
    fn possible(&self) -> PositionT {
        (self.mask + BOTTOM_MASK) & BOARD_MASK
    }

    /*
     * Return a bitmask of the possible winning positions for the current player
     */
    fn winning_position(&self) -> PositionT {
        compute_winning_position(self.current_position, self.mask)
    }

    /*
     * Return a bitmask of the possible winning positions for the opponent
     */
    fn opponent_winning_position(&self) -> PositionT {
        compute_winning_position(self.current_position ^ self.mask, self.mask)
    }

    /*
     * Return a bitmap of all the possible next moves that do not lose in one turn.
     * A losing move is a move leaving the possibility for the opponent to win directly.
     *
     * Warning this function is intended to test position where you cannot win in one turn
     * If you have a winning move, this function can miss it and prefer to prevent the opponent
     * to make an alignment.
     */
    pub fn possible_non_losing_moves(&self) -> PositionT {
        debug_assert!(!self.can_win_next());
        let mut possible_mask = self.possible();
        let opponent_win = self.opponent_winning_position();
        let forced_moves = possible_mask & opponent_win;
        if forced_moves != 0 {
            // check if there is more than one forced move
            if (forced_moves & (forced_moves - 1)) != 0 {
                // the opponnent has two winning moves and you cannot stop him
                return 0;
            } else {
                // enforce to play the single forced move
                possible_mask = forced_moves;
            }
        }
        // avoid to play below an opponent winning spot
        possible_mask & !(opponent_win >> 1)
    }

    /*
     * Score a possible move.
     *
     * @param move, a possible move given in a bitmap format.
     *
     * The score we are using is the number of winning spots
     * the current player has after playing the move.
     */
    pub fn move_score(&self, move_bit: PositionT) -> u32 {
        (compute_winning_position(self.current_position | move_bit, self.mask)).count_ones()
    }

    /*
     * return true if current player can win next move
     */
    pub fn can_win_next(&self) -> bool {
        (self.winning_position() & self.possible()) != 0
    }

    /*
     * Indicates whether the current player wins by playing a given column.
     * This function should never be called on a non-playable column.
     * @param col: 0-based index of a playable column.
     * @return true if current player makes an alignment by playing the corresponding column col.
     */
    pub fn is_winning_move(&self, col: usize) -> bool {
        (self.winning_position() & self.possible() & column_mask(col)) != 0
    }

    /*
     * @return a bitmask where at least four bits are connected
     */
    pub fn bitmask_of_win(&self) -> PositionT {
        let last_position = self.current_position ^ self.mask;
        compute_winning_position_helper(last_position) & last_position
    }
}

impl fmt::Debug for Position {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        if f.alternate() {
            write!(f, "{}", bitmask_to_string(self.readable_key()))
        } else {
            write!(f, "Position::from_key(0x{:x})", self.key())
        }
    }
}
