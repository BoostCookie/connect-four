use super::constants::{HEIGHT, WIDTH};
use super::transposition_table::TranspositionTable;
use super::position::{Position, column_mask};
use super::move_sorter::MoveSorter;

pub struct AnalyseIterator<'a> {
    solver: &'a mut Solver,
    position: &'a Position,
    weak: bool,
    cur_column: usize,
}

impl Iterator for AnalyseIterator<'_> {
    type Item = Option<i32>;

    fn next(&mut self) -> Option<Option<i32>> {
        if self.cur_column < WIDTH {
            self.cur_column += 1;
            if self.position.can_play(self.cur_column - 1) {
                if self.position.is_winning_move(self.cur_column - 1) {
                    Some(Some(((WIDTH * HEIGHT) as i32 + 1 - self.position.nb_moves()) / 2))
                } else {
                    let mut pos2 = self.position.clone();
                    pos2.play_col(self.cur_column - 1);
                    Some(Some(-self.solver.solve(&pos2, self.weak)))
                }
            } else {
                Some(None)
            }
        } else {
            None
        }
    }
}

pub struct Solver {
    trans_table: TranspositionTable,
    // column exploration order
    column_order: [usize; WIDTH],
    // TODO: Opening Book, Node Counter?
}

const MIN_SCORE: i32 = -((WIDTH * HEIGHT) as i32) / 2 + 3;
const MAX_SCORE: i32 = ((WIDTH * HEIGHT) as i32 + 1) / 2 - 3;

impl Solver {
    pub fn new() -> Solver {
        let mut column_order = [0; WIDTH];
        // initialise the column exploration order, starting with center columns
        for i in 0..(WIDTH as i32) {
            // example for WIDTH=7: columnOrder = {3, 4, 2, 5, 1, 6, 0}
            column_order[i as usize] = (WIDTH as i32 / 2 + (1 - 2 * (i % 2)) * (i + 1) / 2) as usize;
        }
        Solver {
            trans_table: TranspositionTable::new(),
            column_order,
        }
    }

    /*
     * Reccursively score connect 4 position using negamax variant of alpha-beta algorithm.
     * @param: position to evaluate, this function assumes nobody already won and
     *         current player cannot win next move. This has to be checked before
     * @param: alpha < beta, a score window within which we are evaluating the position.
     *
     * @return the exact score, an upper or lower bound score depending of the case:
     * - if actual score of position <= alpha then actual score <= return value <= alpha
     * - if actual score of position >= beta then beta <= return value <= actual score
     * - if alpha <= actual score <= beta then return value = actual score
     */
    fn negamax(&mut self, pos: &Position, mut alpha: i32, mut beta: i32) -> i32 {
        debug_assert!(alpha < beta);
        debug_assert!(!pos.can_win_next());

        let possible = pos.possible_non_losing_moves();
        // if no possible non losing move, opponent wins next move
        if possible == 0 {
            return -((WIDTH * HEIGHT) as i32 - pos.nb_moves()) / 2;
        }

        if pos.nb_moves()>= (WIDTH * HEIGHT) as i32 - 2 {
            return 0;
        }

        // lower bound of score as opponent cannot win next move
        let mut min = -((WIDTH * HEIGHT) as i32 - 2 - pos.nb_moves()) / 2;
        if alpha < min {
            // there is no need to keep alpha below our min possible score.
            alpha = min;
            // prune the exploration if the [alpha;beta] window is empty.
            if alpha >= beta {
                return alpha;
            }
        }

        // upper bound of our score as we cannot win immediately
        let mut max = ((WIDTH * HEIGHT) as i32 - 1 - pos.nb_moves()) / 2;
        if beta > max {
            // there is no need to keep beta above our max possible score.
            beta = max;
            // prune the exploration if the [alpha;beta] window is empty.
            if alpha >= beta {
                return beta;
            }
        }

        let key = pos.key();
        let val = self.trans_table.get(key);
        if val != 0 {
            // we have a lower bound
            if val > MAX_SCORE - MIN_SCORE + 1 {
                min = val + 2 * MIN_SCORE - MAX_SCORE - 2;
                if alpha < min {
                    // there is no need to keep alpha below our min possible score.
                    alpha = min;
                    // prune the exploration if the [alpha;beta] window is empty.
                    if alpha >= beta {
                        return alpha;
                    }
                }
            } else {
                // we have an upper bound
                max = val + MIN_SCORE - 1;
                if beta > max {
                    // there is no need to keep beta above our max possible score.
                    beta = max;
                    // prune the exploration if the [alpha;beta] window is empty.
                    if alpha >= beta {
                        return beta;
                    }
                }
            }
        }

        let mut moves = MoveSorter::new();
        for i in (0..WIDTH).rev() {
            let move_bit = possible & column_mask(self.column_order[i]);
            if move_bit != 0 {
                moves.add(move_bit, pos.move_score(move_bit));
            }
        }

        for next in moves {
            let mut pos2 = pos.clone();
            // It is the opponent's turn in pos2 position after current player plays x column
            pos2.play(next);
            // explore opponent's score within [-beta;-alpha] windows
            let score = -self.negamax(&pos2, -beta, -alpha);

            // no need to have good precision for score better than beta (opponent's score worse than -beta)
            // no need to check for score worse than alpha (opponent's score worse better than -alpha)
            if score >= beta {
                // save the lower bound of the position
                self.trans_table.put(key, score + MAX_SCORE - 2 * MIN_SCORE + 2);
                // prune the exploration if we find a possible move better than what we were looking for
                return score;
            }
            if score > alpha {
                // reduce the [alpha;beta] window for next exploration, as we only
                // need to search for a position that is better than the best so far.
                alpha = score;
            }
        }

        self.trans_table.put(key, alpha - MIN_SCORE + 1);
        alpha
    }

    pub fn solve(&mut self, pos: &Position, weak: bool) -> i32 {
        if pos.can_win_next() {
            // check if win in one move as the Negamax function does not support this case.
            return ((WIDTH * HEIGHT) as i32 + 1 - pos.nb_moves()) / 2;
        }
        let mut min = -((WIDTH * HEIGHT) as i32 - pos.nb_moves()) / 2;
        let mut max = ((WIDTH * HEIGHT) as i32 + 1 - pos.nb_moves()) / 2;
        if weak {
            min = -1;
            max = 1;
        }

        // iteratively narrow the min-max exploration window
        while min < max {
            let mut med = min + (max - min) / 2;
            if med <= 0 && min / 2 < med {
                med = min / 2;
            } else if med >= 0 && max / 2 > med {
                med = max / 2;
            }
            // use a null depth window to know if the actual score is greater or smaller than med
            let r = self.negamax(pos, med, med + 1);
            if r <= med {
                max = r;
            } else {
                min = r;
            }
        }
        min
    }

    pub fn analyse<'a>(&'a mut self, pos: &'a Position, weak: bool) -> AnalyseIterator {
        AnalyseIterator {
            solver: self,
            position: pos,
            weak,
            cur_column: 0,
        }
    }
}
