#![feature(test)]

#[cfg(test)]
mod tests;

mod constants;
mod position;
mod solver;
mod move_sorter;
mod transposition_table;

use std::io;
use std::io::Write;
use rand::seq::SliceRandom;
use std::time::Instant;
use constants::{HEIGHT, WIDTH};
use position::Position;
use solver::Solver;
use termcolor::{Color, ColorSpec, ColorChoice, StandardStream, WriteColor};

fn print_position(position: &Position) {
    let mut stdout = StandardStream::stdout(ColorChoice::Always);
    for col in 1..=WIDTH {
        print!(" {}", col);
    }
    print!("\n ");
    for _ in 0..(2 * WIDTH - 1) {
        print!("_");
    }
    println!(" ");
    let (red_positions, yellow_positions) = position.positions();
    let won_positions = position.bitmask_of_win();
    for row in (0..HEIGHT).rev() {
        print!("|");
        for col in 0..WIDTH {
            let cell = 1 << (row + col * (HEIGHT + 1));
            let mut c = if (red_positions & cell) != 0 {
                stdout.set_color(ColorSpec::new().set_fg(Some(Color::Red))).unwrap();
                player_char(1)
            } else if (yellow_positions & cell) != 0 {
                stdout.set_color(ColorSpec::new().set_fg(Some(Color::Yellow))).unwrap();
                player_char(2)
            } else {
                ' '
            };
            if (won_positions & cell) != 0 {
                stdout.set_color(ColorSpec::new().set_fg(Some(Color::Blue))).unwrap();
                c.make_ascii_uppercase();
            }
            print!("{}", c);
            stdout.set_color(ColorSpec::new().set_fg(None)).unwrap();

            if col != WIDTH - 1 {
                print!(" ");
            }
        }
        println!("|");
    }
    print!(" ");
    for _ in 0..(2 * WIDTH - 1) {
        print!("‾");
    }
    println!(" ");
}

fn player_char(player: u8) -> char {
    match player {
        1 => 'x',
        _ => 'o',
    }
}

fn other_player(player: u8) -> u8 {
    match player {
        1 => 2,
        _ => 1,
    }
}

fn print_engine_assistance(solver: &mut Solver, position: &Position) {
    println!("Calculating...");
    for score_opt in solver.analyse(position, false) {
        match score_opt {
            None => print!("  "),
            Some(score) => print!("{: >2}", score),
        }
        io::stdout().flush().unwrap();
    }
    println!();
}

fn play_game(human_players: &[bool; 2], solver: &mut Solver) {
    let weak = false;
    let mut position = Position::empty();
    print_position(&position);

    let mut winner = 0;

    let mut player = 2;
    for ply in 0..(WIDTH * HEIGHT) {
        player = other_player(player);
        println!("Player {}:", player_char(player));

        let col = if human_players[(player - 1) as usize] {
            loop {
                let mut input = String::new();
                io::stdin().read_line(&mut input).unwrap();
                let proposed_col = match input.trim() {
                    "help" => {
                        print_engine_assistance(solver, &position);
                        continue;
                    },
                    s => {
                        match s.parse::<usize>() {
                            Ok(n) => {
                                if n > WIDTH || n == 0 {
                                    println!("Invalid. Please give a valid column number between 1 and {}", WIDTH);
                                    continue;
                                }
                                n - 1
                            },
                            Err(_) => {
                                println!("Invalid. Please give a valid column number between 1 and {}", WIDTH);
                                continue;
                            },
                        }
                    },
                };
                if !position.can_play(proposed_col) {
                    println!("Column not playable. Choose a non-filled column.");
                    continue;
                }
                break proposed_col;
            }
        } else {
            println!("Calculating...");
            let mut max_score = i32::MIN;
            let mut max_score_cols = vec![];
            for (col, score_opt) in solver.analyse(&position, weak).enumerate() {
                match score_opt {
                    None => print!("  "),
                    Some(score) => {
                        print!("{: >2}", score);
                        if score > max_score {
                            max_score = score;
                            max_score_cols = vec![col];
                        } else if score == max_score {
                            max_score_cols.push(col);
                        }
                    },
                };
                io::stdout().flush().unwrap();
            }
            println!();
            *max_score_cols.choose(&mut rand::thread_rng()).unwrap()
        };

        if position.is_winning_move(col) {
            winner = (ply % 2) + 1;
        }
        position.play_col(col);
        print_position(&position);
        if winner != 0 {
            break;
        }
    }

    if winner == 0 {
        println!("It is a tie.");
    } else {
        println!("The winner is {}.", player_char(winner as u8));
    }
}

fn interactive_analysis(solver: &mut Solver) {
    loop {
        let mut line = String::new();
        io::stdin().read_line(&mut line).unwrap();
        let mut pos = Position::empty();
        for c in line.trim().chars() {
            let col = c.to_digit(10).unwrap() as usize - 1;
            pos.play_col(col);
        }
        print_position(&pos);
        let now = Instant::now();
        print_engine_assistance(solver, &pos);
        println!("It took {} ms to calculate.", now.elapsed().as_millis());
    }
}

fn main() {
    let mut solver = Solver::new();
    loop {
        println!("Select playmode [hh,ha,ah,aa,analysis].");
        let human_players = loop {
            let mut input = String::new();
            io::stdin().read_line(&mut input).unwrap();
            match input.trim().to_lowercase().as_str() {
                "aa" => break [false, false],
                "ah" => break [false, true],
                "ha" => break [true, false],
                "hh" => break [true, true],
                "analysis" => interactive_analysis(&mut solver),
                _ => {
                    println!("Invalid. Write for example ha for Player 1 human, Player 2 android.");
                    continue;
                }
            };
        };
        play_game(&human_players, &mut solver);
    }
}
