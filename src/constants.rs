pub const HEIGHT: usize = 6;
pub const WIDTH: usize = 7;
// the table size is the next prime after 2^TABLE_LOG_SIZE
pub const TABLE_LOG_SIZE: usize = 24;
