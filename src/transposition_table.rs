use super::constants::{WIDTH, HEIGHT, TABLE_LOG_SIZE};
use super::position::PositionT;

const fn max(num1: isize, num2: isize) -> isize {
    if num1 > num2 {
        num1
    } else {
        num2
    }
}
pub trait PartialKeyTHelperTrait<const N: usize> {
    type Ty;
}
impl PartialKeyTHelperTrait<8> for () {
    type Ty = u8;
}
impl PartialKeyTHelperTrait<16> for () {
    type Ty = u16;
}
impl PartialKeyTHelperTrait<32> for () {
    type Ty = u32;
}
impl PartialKeyTHelperTrait<64> for () {
    type Ty = u64;
}
impl PartialKeyTHelperTrait<128> for () {
    type Ty = u128;
}
pub type PartialKeyT = <() as PartialKeyTHelperTrait<{
    let size = (max((WIDTH * (HEIGHT + 1)) as isize - TABLE_LOG_SIZE as isize, 0) as usize).next_power_of_two();
    assert!(size <= 128, "PartialKeyT cannot be larger than 128 bits");
    if size >= 8 {
        size
    } else {
        8
    }
},>>::Ty;

/*
 * util functions to compute next prime at compile time
 */
const fn med(min: usize, max: usize) -> usize {
    (min + max) / 2
}
/*
 * tells if an integer n has a a divisor between min (inclusive) and max (exclusive)
 */
const fn has_factor(n: usize, min: usize, max: usize) -> bool {
    if min * min > n {
        false // do not search for factor above sqrt(n)
    } else if min + 1 >= max {
        n % min == 0
    } else {
        has_factor(n, min, med(min, max)) || has_factor(n, med(min, max), max)
    }
}

// return next prime number greater or equal to n.
// n must be >= 2
const fn next_prime(n: usize) -> usize {
    if has_factor(n, 2, n) {
        next_prime(n + 1)
    } else {
        n
    }
}

const TABLE_SIZE: usize = next_prime(1 << TABLE_LOG_SIZE);

fn index(key: PositionT) -> usize {
    key as usize % TABLE_SIZE
}

fn partial_key(key: PositionT) -> PartialKeyT {
    key as PartialKeyT
}

/*
 * Transposition Table is a simple hash map with fixed storage size.
 * In case of collision we keep the last entry and overide the previous one.
 * We keep only part of the key to reduce storage, but no error is possible thanks to Chinese theorem.
 *
 * The number of stored entries is a power of two that is defined at compile time.
 * We also define size of the entries and keys to allow optimisation at compile time.
 */
pub struct TranspositionTable {
    partial_keys: Vec<PartialKeyT>,
    values: Vec<u8>,
}

impl TranspositionTable {
    pub fn new() -> TranspositionTable {
        TranspositionTable {
            partial_keys: vec![0; TABLE_SIZE],
            values: vec![0; TABLE_SIZE],
        }
    }

    /*
     * Store a value for a given key
     * @param key: must be less than key_size bits.
     * @param value: must be less than 8 bits. null (0) value is used to encode missing data
     */
    pub fn put(&mut self, key: PositionT, value: i32) {
        let pos = index(key);
        self.partial_keys[pos] = partial_key(key); // key is truncated
        self.values[pos] = value as u8;
    }

    /*
     * Get the value of a key
     * @param key: must be less than key_size bits.
     * @return the offset value if the key is present, 0 otherwise.
     */
    pub fn get(&self, key: PositionT) -> i32 {
        let pos = index(key);
        if self.partial_keys[pos] == partial_key(key) {
            self.values[pos] as i32
        } else {
            0
        }
    }
}
